function Hamburger(size, stuffing) {
  try {
    if (size === undefined) {
      throw new Error('Enter correct Hamburger.size pls !')
    }
    this.size = size;
    if (stuffing === undefined) {
      throw new Error('Enter correct Hamburger.stuffing pls !')
    }
    this.stuffing = stuffing;
  } catch (e) {
    console.error(e)
  }
  
  this.topping = [];

}

Hamburger.SIZE_SMALL = {
  price: 50,
  cal: 20,
  size: 'small',
  name: 'SIZE_SMALL'
}
Hamburger.SIZE_LARGE = {
  price: 100,
  cal: 40,
  size: 'large',
  name: 'SIZE_LARGE'
}
Hamburger.STUFFING_CHEESE = {
  price: 10,
  cal: 20,
}
Hamburger.STUFFING_SALAD = {
  price: 20,
  cal: 5,
}
Hamburger.STUFFING_POTATO = {
  price: 15,
  cal: 10,
}
Hamburger.TOPPING_MAYO = {
  price: 15,
  cal: 0,
  name: 'TOPPING_MAYO'
}
Hamburger.TOPPING_SPICE = {
  price: 20,
  cal: 5,
  name: 'TOPPING_SPICE'
}

this.topping = [];

Hamburger.prototype.addTopping = function (topp) {
  try {
    if (!topp) {
      throw new Error('Enter correct topping pls');
    }

    if (this.topping.find((x) => x == topp)) {
      throw new Error('You have already have added this topping !')
    }
    this.topping.push(topp);
  } catch (e) {
    console.error(e)
  }
}

Hamburger.prototype.removeTopping = function (topp) {
  try {
    if (!topp) {
      throw new Error('Enter correct topping pls');
    }

    let index = this.topping.indexOf(topp);
    console.log('index', index)
    if (index < 0) {
      throw new Error('This topping did not add!')
    }
    this.topping.splice(index, 1);
  } catch (e) {
    console.error(e)
  }
}

Hamburger.prototype.getToppings = function () {
  console.log('topping', this.topping)
}

Hamburger.prototype.getName = function () {
  console.log(this.size.name)
}

Hamburger.prototype.calculatePrice = function () {
  try{
  let toppingPrice = 0;
  let price = 0;

  this.topping.forEach((el) => {

    if (typeof el.price !== 'number') {
      throw new Error('Enter correct topping pls !')
    }
    toppingPrice += el.price;
  })
  if (!this.size || !this.stuffing) {
    throw new Error('Enter correctly value of Hamburger !!!')
  }
  
  price += (this.size.price + this.stuffing.price + toppingPrice);
  return price
}catch(e){
  console.error(e)
}
};

Hamburger.prototype.calculateCal = function () {
  try{
  let cal = 0;
  let toppingCal = 0;
  this.topping.forEach((el) => {
    if (typeof el.cal !== 'number') {
      throw new Error('Enter correct topping pls !')
    }
    toppingCal += el.cal
  })

  if (!this.size || !this.stuffing) {
    throw new Error('Enter correctly value of Hamburger !!!')
  }
  cal += (this.size.cal + this.stuffing.cal + toppingCal);

  return cal
}catch(e){
  console.error(e)
}
}

let ham = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

ham.addTopping(Hamburger.TOPPING_SPICE);
ham.addTopping(Hamburger.TOPPING_SPICE);
ham.addTopping(Hamburger.TOPPING_MAYO);
ham.addTopping(Hamburger.TOPPING_MAYO);
ham.removeTopping(Hamburger.TOPPING_SPICE);

// ham.getName();
ham.getToppings();

console.log('ham.calculatePrice', ham.calculatePrice());
console.log('ham.calculateCal', ham.calculateCal());

console.log('ham', ham);